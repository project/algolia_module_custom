CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

This module is an extension of Algolia search API module, it adds search
feature.

* For a full description of the module, visit the project page:
https://www.drupal.org/project/algolia_module_custom

* To submit bug reports and feature suggestions, or to track changes:
https://www.drupal.org/project/issues/algolia_module_custom


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

* Install the Algolia search module module as you would normally install a
contributed Drupal module.
Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

1. Navigate to Administration > Extend and enable the module.
2. Navigate to Administration > Configuration > Algolia parametres > Search
   Settings for configuration.


MAINTAINERS
-----------

* asmaa khalfi (asmaakhalfi) - https://www.drupal.org/u/asmaakhalfi

Supporting organizations:

* Fullwave - https://www.drupal.org/fullwave
